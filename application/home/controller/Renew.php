<?php

// +----------------------------------------------------------------------
// | KyxsCMS Renew Novel Extend
// +----------------------------------------------------------------------
// | Copyright (c) Tealun Du All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Tealun Du
// +----------------------------------------------------------------------

namespace app\home\controller;

use app\common\controller\Base;
use think\Db;
use app\api\controller\Source;


class Renew extends Base
{
    /**
     * 自动更新小说
     * 需配合服务器自动任务进行全量更新，或手动触发全量更新、指定范围（数据页码）更新
     * 服务器自动更新请在[计划任务]中设置[任务类型]为[访问URL](*此为宝塔设置方法，其他控制面板请咨询服务器提供商，暂未提供其他指引，后期会完善手动触发功能)
     * URL地址 http://www.yourwebsite.com/Home/Renew/index?renew_key=yourRenewKey
     * @param string $inKey
     * @param int $startPage
     * @param int $page
     * @param int $limit
     * @param bool $type
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function index($inKey='',$startPage = 1,$page = 0,$limit = 50,$type=false){

        $renewKey = Db::name("Config")
            ->where('name','=','auto_renew_key')
            ->value('value');
        if(!$renewKey) exit('未设置更新密钥');

        $getKey = input('get.renew_key');
        $key = $inKey!=''?$inKey:$getKey;
        if(!$key || $key !== $renewKey) exit('没有刷新密钥或刷新密钥不正确');//判断有无传入刷新密钥，密钥是否正确

        $dir = './runtime/log/renew';
        if(!is_dir($dir)) mkdir($dir, 0755);

        //检测任务锁定
        $lockFile = $dir.'/processing.lock';
        if(is_file($lockFile)) exit('正在执行刷新，请稍后再试');

        /** @var string $string 开始记录日志*/
        $typeName = $type?'手动更新':'全量更新';
        $time = time();
        $string =  date('Y-m-d H:i:s',$time).'开始执行'.$typeName.'任务'.PHP_EOL;

        $count =Db::name('novel')
            ->where('serialize','=',0)//状态为连载的
            ->where('status','=',1)//状态为正常的
            ->count();
        $totalPage = ceil($count/$limit);
        $page = $page && $page <= $totalPage
                ? $page : $totalPage;//如果有传入结束页面（用于管理后台），赋值传入值，传入页面大于总页面或未传入则取全部页码最后页

        $string .= '----------------------------------------'.PHP_EOL;
        $string .= '系统可刷新'.$count.'条数据，共有'.ceil($count/$limit).'页,每页'.$limit.'条数据'.PHP_EOL;
        $string .= '----------------------------------------'.PHP_EOL;

        /** @var int $r 最后一页数据量*/
        $r = $count%$limit;
        //重新整理本次任务数据总量，根据开始页面和结束页面计算，最后一页数据不等于$limit时取最后一页数量
        $count = $r && $page == ceil($count/$limit)
                 ? ($page - $startPage)*$limit+$r : ($page - $startPage +1)*$limit;

        $string .= '----------------------------------------'.PHP_EOL;
        $string .= '本次任务尝试更新'.$count.'条数据'.PHP_EOL;
        $string .= '从第'.$startPage.'页到第'.$page.'页,每页'.$limit.'条数据'.PHP_EOL;
        $string .= '----------------------------------------'.PHP_EOL;

        $file = $dir.'/record'.$time.'.txt';
        file_put_contents($file,$string); //写入日志头部

        /******** background process starts here ********/
        ignore_user_abort(true);//在关闭连接后，继续运行php脚本
        /******** background process ********/
        set_time_limit(0); //no time limit，不设置超时时间（根据实际情况使用）
        /******** Rest of your code starts here ********/

        //正常情况下，网络搜集到的PHP长时间运行程序都是设置上述内容，以上如果设置后仍然超时返回502，考虑服务器运行环境 可以加入如下 在FASTCGI模式下的一些设置 避免超时
        ini_set('memory_limit', '-1');// 避免内存不足

        // 该方法是FPM提供的方法，只能运行在FastCGI模式下，在CLI模式或者是模块模式等非FPM模式下的话，会报错的，需要加上以下代码才可以
        if (!function_exists("fastcgi_finish_request")) {
            function fastcgi_finish_request()  {
            }
        }

        fastcgi_finish_request();// 完成响应, 关闭连接

        file_put_contents($file,'开始刷新'.PHP_EOL,FILE_APPEND);//标记开始

        /** @var object $Source 调用狂雨系统小说刷新类*/
        $Source = new Source();

        /** @var int $error 定义待出错总数*/
        $error = 0;

        /** @var int $error_page 定义出错页码数*/
        $error_page = 0;

        /** @var  $access 定义更新成功小说计数变量*/
        $access = 0;

        /** @var int $p 定义循环页数变量 开始分页获取小说并遍历更新*/
        for($p=$startPage;$p<=$page;$p++){

            file_put_contents($file,'页码'.$p.''.PHP_EOL,FILE_APPEND);
            sleep(2);//每更新一页数据，暂停2秒

            $ids =Db::name('novel')//获取分页内容
                ->where('serialize','=',0)//状态为连载的
                ->where('status','=',1)//状态为正常的
                ->field('id,title')->page($p,$limit)->select();

            if(!$ids){
                file_put_contents($file,'ERROR 未获取到第'.$p.'页更新列表'.PHP_EOL,FILE_APPEND);
                file_put_contents($file,'------------------------------------'.PHP_EOL,FILE_APPEND);
                $error_page++;
                $error = $error+$limit;
                break;
            }

            foreach ($ids as $k => $v){//遍历分页内容更新
                //设置任务锁定并记录当前执行信息
                $re = $Source->index($v['id']);
                if($re){//有更新记录状态和详细数据
                    file_put_contents($file,date('Y-m-d H:i:s',time()).' | 更新成功：['.$v['title'].']('.$v['id'].') √ ok.'.PHP_EOL,FILE_APPEND);
                    $access++;
                }else{//无更新，仅记录状态
                    file_put_contents($file,date('Y-m-d H:i:s',time()).' | 暂无新章节：['.$v['title'].']('.$v['id'].')'.PHP_EOL,FILE_APPEND);
                }
                $status = $re?'√ Ok 新章节更新成功':'-_-! 暂无新章节';
                file_put_contents($lockFile,serialize([
                    'all_novel'=>$count,
                    'now_novel'=>($p-$startPage)*$limit+($k+1),
                    'novel'=>[
                        'id'=>$v['id'],
                        'title'=>$v['title'],
                        'status'=>$status
                    ]]));

            }

            file_put_contents($file,'------------------------------------'.PHP_EOL,FILE_APPEND);
        }

        file_put_contents($file,'----------------------------------------'.PHP_EOL,FILE_APPEND);//完成日志
        file_put_contents($file,'本次更新任务完成，共计更新'.$access.'本书'.PHP_EOL,FILE_APPEND);
        file_put_contents($file,'----------------------------------------'.PHP_EOL,FILE_APPEND);//完成日志

        /** 完成后写入更新成功数据 供前端查阅 */
        $finishTime = time();
        $dataFile = $dir.'/renew_data'.$time.'.php';
        $total_time = $finishTime - $time;
        $total_time = self::secondToWord($total_time);
        $data = [
            'total_page'=>$page-$startPage+1,
            'limit'=>$limit,
            'error_page'=>$error_page,
            'renew_time'=>$time,
            'finish_time'=>$finishTime,
            'total_time'=>$total_time,
            'total_num' =>$count,
            'renew_num'=>$access,
            'error_num'=>$error,
            'type_text'=>$type?'手动更新':'自动更新'
        ];
        file_put_contents($dataFile,serialize($data));

        //解除任务锁定
        unlink($lockFile);

        /** 释放变量 */
        unset($ids,$access,$dataFile,$file,$data,$type,$count,$time,$finishTime,$page,$totalPage,$dir,$inKey,$lockFile);
        exit();
    }

    public static function secondToWord($s=0){
        //计算分钟
        //算法：将秒数除以60，然后下舍入，既得到分钟数
        $m    =    floor($s/60);
        //计算秒
        //算法：取得秒%60的余数，既得到秒数
        $s    =    $s%60;
        //计算小时
        //算法一样
        $h=0;
        if($m >60){
            $h    =    floor($m/60);
            $m = $h%60;
        }
        $d=0;
        if($h>24){
            $d = floor($h/24);
            $h = $d%24;
        }
        //如果只有一位数，前面增加一个0
        $s = (strlen($s)==1)?'0'.$s:$s;
        $s = $s?$s.'秒':'';
        $m = $m?$m.'分':'';
        $d = $d?$d.'天':'';
        $h = $h?$h.'小时':'';
        return $d.$h.$m.$s;
    }
}
